import 'package:flutter/material.dart';

//https://www.youtube.com/watch?v=GLSG_Wh_YWc&ab_channel=Academind
  
  // Mount the widget object to the screen
  // Construct an object based on the class MyApp with MyApp()
  // a function with 1 line can be written as //void main() => runApp(MyApp());
  void main() {
    runApp(MyApp()); 
  }


class MyApp extends StatelessWidget {
  // Returns a widget tree
  @override
  Widget build(BuildContext context) {
    // A Widget needs to return another widget until your reach a widget that ships with Flutter
    //MaterialApp widget ships with Flutter, it is a wrapper for the entire app
    //Scaffold widget creates a new page in the app with white background
    // Inside of the Scaffoold there will be an AppBar widget
    // Text() accepts a string positional parameter with no name, which will be displayed in the widget
    
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('EasyList'),
        ),
        //child stores the content of the Card
        //children array holds Widgets
        body: Card(child: Column(children: <Widget>[
          // Constructor which creates an Image widget configures to load an asset
          Image.asset('assets/food.jpg'),
          Text('Food Paradise')
        ],),)
      ),
    );
  }
}
